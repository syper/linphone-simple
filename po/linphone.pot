# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the linphone package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: linphone\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-18 21:55+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../qml/SettingsPage.qml:15 ../qml/components/MainHeader.qml:29
msgid "Settings"
msgstr ""

#: ../qml/SettingsPage.qml:41
msgid "Logged in as:"
msgstr ""

#: ../qml/SettingsPage.qml:41 ../qml/SettingsPage.qml:76
msgid "Log in with Existing Account"
msgstr ""

#: ../qml/SettingsPage.qml:46 ../qml/components/ActiveAccount.qml:20
#: ../qml/Main.qml:102 ../qml/Main.qml:257
msgid "offline"
msgstr ""

#: ../qml/SettingsPage.qml:46
msgid "Connecting"
msgstr ""

#: ../qml/SettingsPage.qml:46
msgid "or create a free Linphone SIP account online"
msgstr ""

#: ../qml/SettingsPage.qml:55
msgid "Log out"
msgstr ""

#: ../qml/SettingsPage.qml:55 ../qml/WelcomePage.qml:74
msgid "Log in"
msgstr ""

#: ../qml/SettingsPage.qml:67
msgid "Create"
msgstr ""

#: ../qml/SettingsPage.qml:85
msgid "Stop Linphone when close"
msgstr ""

#: ../qml/SettingsPage.qml:90
msgid "Will not run in the background"
msgstr ""

#: ../qml/SettingsPage.qml:105
msgid "Keep display on"
msgstr ""

#: ../qml/SettingsPage.qml:122
msgid "Use dark theme"
msgstr ""

#: ../qml/components/CommonDomains.qml:8
msgid "Add a known SIP domain:"
msgstr ""

#: ../qml/components/ContactIcon.qml:35
msgid "Contact"
msgstr ""

#: ../qml/components/MainHeader.qml:20
msgid "Info"
msgstr ""

#: ../qml/components/ListOfContacts.qml:36
msgid "Delete"
msgstr ""

#: ../qml/components/ListOfContacts.qml:47
msgid "Add"
msgstr ""

#: ../qml/components/DelFavContact.qml:11
#: ../qml/components/DelFavContact.qml:18
msgid "Delete Contact"
msgstr ""

#: ../qml/components/DelFavContact.qml:12
msgid "Delete favorite contact"
msgstr ""

#: ../qml/components/DelFavContact.qml:30
#: ../qml/components/AddFavContact.qml:108
msgid "Cancel"
msgstr ""

#: ../qml/components/AddFavContact.qml:16
msgid "Add Favorite Contact"
msgstr ""

#: ../qml/components/AddFavContact.qml:54
msgid "Contact <b>Name</b>"
msgstr ""

#: ../qml/components/AddFavContact.qml:65
msgid "Full <b>SIP Address</b>"
msgstr ""

#: ../qml/components/AddFavContact.qml:93
msgid "Add Contact"
msgstr ""

#: ../qml/components/AccountsHeader.qml:18
msgid "Information"
msgstr ""

#: ../qml/WelcomePage.qml:12
msgid "First Run"
msgstr ""

#: ../qml/WelcomePage.qml:58
msgid "Welcome to Linphone"
msgstr ""

#: ../qml/WelcomePage.qml:66
msgid "Do you have an existing SIP account? You can log in to use it."
msgstr ""

#: ../qml/WelcomePage.qml:82
msgid ""
"Do you want to create a new SIP account with Linphone? Tap to open Linphone "
"website and sign up."
msgstr ""

#: ../qml/WelcomePage.qml:89
msgid "Sign up"
msgstr ""

#: ../qml/LinphoneAccount.qml:16
msgid "SIP Accounts"
msgstr ""

#: ../qml/LinphoneAccount.qml:59
msgid "<b>Username</b> of the SIP account"
msgstr ""

#: ../qml/LinphoneAccount.qml:89
msgid ""
"Provider <b>Domain</b>. Usually, what it goes after @ of your SIP address"
msgstr ""

#: ../qml/LinphoneAccount.qml:115
msgid "<b>Password</b> of the SIP account"
msgstr ""

#: ../qml/LinphoneAccount.qml:137
msgid "Login"
msgstr ""

#: ../qml/Keypad.qml:48
msgid "1"
msgstr ""

#: ../qml/Keypad.qml:66
msgid "2"
msgstr ""

#: ../qml/Keypad.qml:83
msgid "3"
msgstr ""

#: ../qml/Keypad.qml:100
msgid "4"
msgstr ""

#: ../qml/Keypad.qml:117
msgid "5"
msgstr ""

#: ../qml/Keypad.qml:134
msgid "6"
msgstr ""

#: ../qml/Keypad.qml:151
msgid "7"
msgstr ""

#: ../qml/Keypad.qml:167
msgid "8"
msgstr ""

#: ../qml/Keypad.qml:184
msgid "9"
msgstr ""

#: ../qml/Keypad.qml:203
msgid "*"
msgstr ""

#: ../qml/Keypad.qml:220
msgid "0"
msgstr ""

#: ../qml/Keypad.qml:221
msgid "+"
msgstr ""

#: ../qml/Keypad.qml:241
msgid "#"
msgstr ""

#: ../qml/Main.qml:173
msgid "SIP address to call"
msgstr ""

#: ../qml/Main.qml:203
msgid "Incoming Call from URL"
msgstr ""

#: ../qml/Main.qml:365
msgid "Dev"
msgstr ""

#: ../qml/Main.qml:373
msgid "Development"
msgstr ""

#: ../qml/Main.qml:385
msgid "Send a command to Linphone"
msgstr ""

#: ../qml/IncomingCall.qml:36
msgid "Incoming Call"
msgstr ""

#: ../qml/IncomingCall.qml:57 ../qml/OutgoingCall.qml:53
msgid "Duration: "
msgstr ""

#: ../qml/IncomingCall.qml:57 ../qml/OutgoingCall.qml:53
msgid " seconds"
msgstr ""

#: ../qml/IncomingCall.qml:128
msgid "Current Call"
msgstr ""

#: ../qml/OutgoingCall.qml:32
msgid "In Call"
msgstr ""

#: ../qml/OutgoingCall.qml:32
msgid "Calling:"
msgstr ""

#: ../about.qml.in:23 ../about.qml.in:37
msgid "About"
msgstr ""

#: ../about.qml.in:51 ../about.qml.in:52
msgid "App Development"
msgstr ""

#: ../about.qml.in:54 ../about.qml.in:55
msgid "Code Used from"
msgstr ""

#: ../about.qml.in:54
msgid "License GPLv2"
msgstr ""

#: ../about.qml.in:55
msgid "Ubuntu Touch Linphone build modified code"
msgstr ""

#: ../about.qml.in:57 ../about.qml.in:58 ../about.qml.in:59 ../about.qml.in:60
#: ../about.qml.in:61 ../about.qml.in:62 ../about.qml.in:63 ../about.qml.in:64
#: ../about.qml.in:65
msgid "Special Thanks to"
msgstr ""

#: ../about.qml.in:57
msgid "for starting it all"
msgstr ""

#: ../about.qml.in:58 ../about.qml.in:59 ../about.qml.in:60 ../about.qml.in:61
#: ../about.qml.in:62 ../about.qml.in:63 ../about.qml.in:64 ../about.qml.in:65
msgid "for his untiringly testing the alpha and beta builds"
msgstr ""

#: ../about.qml.in:67
msgid "Icons"
msgstr ""

#: ../about.qml.in:122
msgid "Version %1. Source %2"
msgstr ""

#: ../about.qml.in:136
msgid "Under License %1"
msgstr ""

#: linphone.desktop.in.h:1
msgid "Linphone"
msgstr ""
